package copofi.landingPage;


import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

public class landingPage_2 {

	public WebDriver driver;

	public landingPage_2(WebDriver driver) {

		this.driver = driver;
	}

	public void executeStep() throws Exception {

		Reporter.log("<b> Description : </b>" + "\n" + "Configure filter for number of bedrooms (Min, Max)");
		Reporter.log("\n");

		checkMinRooms(); // Method calling for Minimum room list validation
		checkMaxRooms(); // Method calling for Maximum room list validation
		validateOnIncorrectMinInputs(); // Validate incorrect input in minimum maximum drop down
		Thread.sleep(2000); // Wait until the error message appears
		refreshPage(); // Refresh the page to flush out the incorrect inputs
	}

	public void checkMinRooms() throws InterruptedException {
		Reporter.log("<b>Verify - checkMinRooms</b>"); // logs displayed in test report
		Reporter.log("\n");
		driver.findElement(By.id("roomDropDown")).click(); // click on room drop down
		driver.findElement(By.id("minBedDropDown")).click(); // click on minimum bed required drop down
		List<WebElement> options = driver.findElements(By.className("btn")); // fetch all the drop down options for room
		ArrayList<String> minOptions = new ArrayList<String>(); // initiate array to store expected room list
		minOptions.add("NO MIN");
		minOptions.add("STUDIO");
		minOptions.add("1");
		minOptions.add("2");
		minOptions.add("3");
		minOptions.add("4");
		minOptions.add("5");
		minOptions.add("6");
		ArrayList<String> availableMin = new ArrayList<String>(); // array list to store actual room list in string 
		for (WebElement option : options) {
			/* loop for iterating options and to get the text value from them to add in to another list*/
			if (option.getText().isEmpty() == false) {
				availableMin.add(option.getText());// add the text value of the elements into an array list
				System.out.println("\n Each option : " + option.getText()); // print values
			}
		}

		for (int i = 0; i < minOptions.size(); i++) {
			/* loop to check expected with actual rooms*/
			Assert.assertTrue(availableMin.contains(minOptions.get(i)),
					minOptions.get(i).toString() + " - Text/Option Not Available"); // check expected and actual room lists match or not

			Reporter.log(minOptions.get(i).toString() + " - Text/Option is Available"); // logs displayed in test report
			Reporter.log("\n");
		}
	}

	public void validateOnIncorrectMinInputs() throws InterruptedException, AWTException {
		/* method to provide invalid input and validate error message*/
		Reporter.log("<b>Verify - validateOnIncorrectMaxMinInputs</b>"); // logs displayed in test report
		Reporter.log("\n");
		driver.findElement(By.id("minBedDropDown")).click(); // click on min bed drop down
		Thread.sleep(500);
		//driver.findElement(By.xpath("//label[text()='2']")).click();
		driver.findElement(By.xpath("//form[contains(@class,'ng-untouched')]/div/div[2]/div/div/div/div/div[1]/div/div/div[4]/label")).click(); // set min dropdown as 2
		
		Thread.sleep(500);
		driver.findElement(By.id("maxBedDropDown")).click(); // click on max bed drop down
		Thread.sleep(500);
		driver.findElement(By.xpath("//form[contains(@class,'ng-untouched')]/div/div[2]/div/div/div/div/div[2]/div/div/div[3]/label")).click(); // set max dropdown as 1
		
		//driver.findElement(By.xpath("//label[text()='1']")).click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//input[@type='text']")).click(); // click on search text box
		driver.findElement(By.xpath("//input[@type='text']")).clear(); // clear search text box value
		driver.findElement(By.xpath("//input[@type='text']")).sendKeys("London"); // enter search keyword
		Thread.sleep(500);
		Robot robot = new Robot(); // Robot class object for handling mouse and keyboard events
		robot.keyPress(KeyEvent.VK_DOWN); // Press the down key 
		robot.keyRelease(KeyEvent.VK_DOWN); // Release down key
		robot.keyPress(KeyEvent.VK_ENTER); // Press the Enter key
		robot.keyRelease(KeyEvent.VK_ENTER); // Release Enter key
		robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); // release mouse
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@aria-live='polite']"))); // Explicit wait for visibility of the error message
		WebElement actualErrormsg = driver.findElement(By.xpath("//div[@aria-live='polite']")); // get the error message
		
		validateOnIncorrectMaxMinInputs(actualErrormsg.getText()); // Validate actual and expected error message
		
		driver.findElement(By.xpath("//div[@aria-live='polite']")).click(); // click on the error message
		driver.findElement(By.id("roomDropDown")).click(); // click on room drop down
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Clear"))); // wait for visibility of the element
		driver.findElement(By.linkText("Clear")).click(); // clear the selected choices
	}

	public void validateOnIncorrectMaxMinInputs(String actualErrormsg) throws InterruptedException, AWTException {
		/* method to validate incorrect input*/ 
		
		String expectedErrorToast = "Value for minimum bedroom should not be bigger than the maximum bedroom";
		
		assertTrue(expectedErrorToast.equalsIgnoreCase(actualErrormsg), "\n Expected error message : "
				+ expectedErrorToast + " does not match actual error message : " + actualErrormsg); // Validate actual and expected error message

		Reporter.log("validateOnIncorrectMaxMinInputs - Passed"); // logs displayed in test report
		Reporter.log("\n");
	}

	public void checkMaxRooms() throws InterruptedException {
		/*method to check maximum rooms list*/
		Reporter.log("<b>Verify - checkMaxRooms</b>");
		Reporter.log("\n");
		driver.findElement(By.id("maxBedDropDown")).click(); // Click on maximum rooms drop down
		Thread.sleep(500);
		List<WebElement> options = driver.findElements(By.className("btn"));  // insert all the list items into an object
		ArrayList<String> maxOptions = new ArrayList<String>(); // make an expected items array list
		maxOptions.add("STUDIO");
		maxOptions.add("1");
		maxOptions.add("2");
		maxOptions.add("3");
		maxOptions.add("4");
		maxOptions.add("5");
		maxOptions.add("6");
		maxOptions.add("NO MAX");
		ArrayList<String> availableMax = new ArrayList<String>();
		for (WebElement option : options) {
			if (option.getText().isEmpty() == false) {
				availableMax.add(option.getText()); // add the actual elements in string to the array
			}
		}
		for (int i = 0; i < maxOptions.size(); i++) {
			assertTrue(availableMax.contains(maxOptions.get(i)),
					maxOptions.get(i).toString() + " - Text/Option Available"); // Validate actual and expected elements
			Reporter.log(maxOptions.get(i).toString() + " - Text/Option is Available"); // logs displayed in test report
			Reporter.log("\n");
		}
	}

	public void refreshPage() {
		String cURL = driver.getCurrentUrl(); // get the current URL
		driver.get(cURL); // hit the current url
	}
}
