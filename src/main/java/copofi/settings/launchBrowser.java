package copofi.settings;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class launchBrowser {

	public WebDriver driver;
	public String userDir = System.getProperty("user.dir"); // get the current workspace path
	public String baseURL; // production: http://34.228.214.228/frontend

	@Parameters({ "browser" }) // passing browser parameter from testng.xml file
	@BeforeTest
	public void openBrowser(String browser) throws InvalidFormatException, IOException {
		/* method used for opening  browser*/
		// System.out.println("System Path - "+userDir);
		baseURL = getURL(); // getURL provided in the Excel file
		System.out.println("baseURL - " + baseURL);

		if (browser.equalsIgnoreCase("firefox")) {
			System.out.println("launching Firefox browser");
			String firefoxdriverPath = userDir + "/Gecko/geckodriver"; // get the firefox driver path from workspace
			System.setProperty("webdriver.gecko.driver", firefoxdriverPath); // set firefoxdriverPath
			driver = new FirefoxDriver(); // initiate firefox driver
			driver.get(baseURL + "/landing"); // hit the landing page
			driver.manage().window().maximize(); // maximize browser window
			driver.findElement(By.xpath("//cookie-law/cookie-law-el/div/div/span/button")).click(); // accept cookie
																									// policy
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // implicitly wait until the URL is opened in browser 
		}

		if (browser.equalsIgnoreCase("chrome")) {
			System.out.println("launching Chrome browser");
			String chromedriverPath = userDir + "/chrome/chromedriver"; // get the chrome driver path from workspace
			System.setProperty("webdriver.chrome.driver", chromedriverPath); // set chromedriverPath
			driver = new ChromeDriver(); // initiate chrome driver
			driver.get(baseURL + "/landing"); // hit the landing page
			driver.manage().window().maximize(); // maximize browser window
			driver.findElement(By.xpath("//cookie-law/cookie-law-el/div/div/span/button")).click(); // accept cookie
																									// policy
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // implicitly wait until the URL is opened in browser 
		}
	}

	public WebDriver getdriver() {

		return driver; // getter method for driver
	}

	public String getBaseURL() {

		return baseURL; // getter method for baseURL
	}

	public String getURL() throws InvalidFormatException, IOException {
		
		/* method for fetching URL from the excel sheet kept in the project workspace*/

		ReadExcel rdexcel = new ReadExcel(); // create read excel object
		String sheetName = "url"; // excel sheet name
		List Result = rdexcel.readSheet(userDir + "/url.xls", sheetName); // get row, column and sheet name

		int totalRow = (Integer) Result.get(0); // get total row
		int totalCol = (Integer) Result.get(1); // get total column
		Sheet sheetTestSuite = (Sheet) Result.get(2); // sheet name

		String cellValue = null;

		for (int i = 0; i <= totalRow; i++) {
			Row rowTestSuite = sheetTestSuite.getRow(i); // get the row at i
			for (int j = 0; j <= totalCol; j++) {

				if (ReadExcel.readCell(rowTestSuite, j) != null) {
					cellValue = ReadExcel.readCell(rowTestSuite, j); // fetch the cell value at column j
				}
			}
		}
		return cellValue; // return the cell value
	}

	@AfterTest
	public void endtest() {

		driver.quit(); // quit current driver session
	}
}
