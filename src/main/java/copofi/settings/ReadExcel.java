package copofi.settings;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel {
   
	/* This method is used to read an excel sheet */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List readSheet(String FilePath, String SheetName) throws IOException, InvalidFormatException {
  	
        Workbook WB = WorkbookFactory.create(new FileInputStream(FilePath)); // WB class object is created 
        Sheet sheetName = WB.getSheet(SheetName); // sheet object created
        ArrayList totalRow = new ArrayList(); // create a list for adding total no. of columns, rows and sheet name
        int colNo = sheetName.getRow(0).getPhysicalNumberOfCells(); // get total no. of columns in the sheet
        System.out.println("No of columns are: "+colNo);
        totalRow.add(sheetName.getLastRowNum()); // add total no. of rows in the sheet
        totalRow.add(colNo); // add total column in the list
        totalRow.add(sheetName); // add sheet name in the list
        return totalRow;
    }

    /* This method is used to read the excel sheet cell data */
    @SuppressWarnings("deprecation")
	public static String readCell(Row rowNo, int cellNo) {
        String Pwd = null;
        Row rowN = rowNo; // local variable for row
        Cell cell = rowN.getCell(cellNo); // get cell value from the row
        if (cell != null) {
            int type = cell.getCellType();
            //Checks the type of contain consist in the cell
            if (type == HSSFCell.CELL_TYPE_STRING) {
                Pwd = cell.getRichStringCellValue().toString(); // get string cell value
            } else if (type == HSSFCell.CELL_TYPE_NUMERIC) {
            	if (DateUtil.isCellDateFormatted(cell)) {
            		DateFormat df = new SimpleDateFormat("DD/MM/YYYY"); 
            	    Date today = cell.getDateCellValue(); 
            	    Pwd = df.format(today); // get date cell value
                } else {
                	int i = (int) cell.getNumericCellValue();
                    Pwd = Integer.toString(i); // get numeric cell value in string
                }
                
            } else if (type == HSSFCell.CELL_TYPE_BOOLEAN) {
                System.out.println(cell.getBooleanCellValue() + " = Boolean Value");// get boolean cell value
            } else if (type == HSSFCell.CELL_TYPE_BLANK) {
                System.out.println(cell.getColumnIndex() + " = BLANK CELL"); // print if blank
            } 
        }
        return Pwd;
    }

}
