package copofi.testRun;

import org.openqa.selenium.WebDriver;

import copofi.landingPage.landingPage_2;

public class landingPageSearch {

	public WebDriver driver;
	public String baseURL;
	public landingPage_2 lp_2;

	public landingPageSearch(WebDriver driver) {

		/* this constructor is user to set the driver so that the driver can be instantiated
		  in some other class and pass the value in this class
		 */
		this.driver = driver; // set driver value
	}

	public void testCaseNo2(String baseURL) throws Exception {
		String expURL = baseURL + "/landing";
		urlCheck(expURL); // check expected url with actual url
		lp_2 = new landingPage_2(driver); // create object for the respective class
		lp_2.executeStep(); // call execute step method to perform test steps
	}

	public void urlCheck(String expectedURL) {
		/* this method is to check if the expected and current url are matching*/
		String cURL = driver.getCurrentUrl(); // get current url

		if (cURL.equals(expectedURL) != true) {
			driver.get(expectedURL); // if current url is not same as expected url then open the the expected url
		}
	}

}
