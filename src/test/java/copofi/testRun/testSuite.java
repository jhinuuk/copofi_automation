package copofi.testRun;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import copofi.settings.launchBrowser;

public class testSuite extends launchBrowser {

	public WebDriver driver;
	public String baseURL;
	public landingPageSearch lps;

	@BeforeClass
	public void setdriver() {

		driver = getdriver(); /* this constructor is user to set the driver so that the driver can be instantiated
		  in some other class and pass the value in this class
		 */
	}
	
	@BeforeClass
	public void setBaseURL() {

		baseURL = getBaseURL(); // get the base URL in launch browser
	}
	
	@Test(priority = 1)
	public void seachLandingPage() throws Exception {
		lps = new landingPageSearch(driver); // create test suite object
		lps.testCaseNo2(baseURL); // call the test case required to be executed 
	}
	
}